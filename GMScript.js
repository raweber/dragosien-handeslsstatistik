// ==UserScript==
// @name     Dragosien Handelsstatistik
// @version  1
// @grant       GM.getValue
// @grant       GM.setValue
// @include *dragosien.de/?t=market&*
// @include *dragosien.de/?&t=market&*
// ==/UserScript==

//Change here the spread
Offer = 0.06;
Demand = 0.06;

//Liste aller Ressourcen
var ResList = ["alraune","bretter","brot","eisen","eisenerz","zauberwasser","faesser","fleisch","gemuese","geschirr","getreide","hanf","holz","honig","kerzen","kohle","leder","lehm","mehl","met","moebel","naegel","obst","papier","sattel","seile","folianten","steine","steinziegel","stoff","vieh","werkzeuge","ziegel"];

//Rechter Seitenrand
var rand = document.getElementById("mainRight");



//Nur im Statistikbereich
if (/tab=13&/.exec(window.location.href))
{
  function GetPrice (ProductName, Hour)
  {
    var SearchString = RegExp (ProductName+" .+\\["+Hour+",(\\d+.\\d+)\\]");
    var SearchStringShort = RegExp (ProductName+" .+\\["+Hour+",(\\d+)\\]");
    if (SearchString.exec(document.getElementById("source").innerHTML)) {return SearchString.exec(document.getElementById("source").innerHTML)[1]}
    else if (SearchStringShort.exec(document.getElementById("source").innerHTML)) {return SearchStringShort.exec(document.getElementById("source").innerHTML)[1]}
    else {alert ("ERROR")}
  }

  if (document.getElementById("footer"))
  {
    //Serverzeit in Array: dd:mm:jjjj:Stunde:Minute:Sekunde
    var Servertime = /Serverzeit: (\d+\.\d+\.\d+ \d+:\d+:\d+) Uhr/.exec(document.getElementById("footer").innerHTML)[1].replace(/\.|\s|:/g, ";").split(";");
    if (Servertime == null) {alert ("ERROR - Handelsstatistik - Find Servertime failed"); return 33;}
  }

  //Laden des letzten Updates
    (async () => {
 			 let LastUpdate = JSON.parse(await GM.getValue('LastUpdate',null));
			 if (LastUpdate == null) {alert("ERROR - Handelsstatistik - Load LastUpdate (Date) failed"); return 33;}
        let Prices = JSON.parse(await GM.getValue('Prices',null));
  		if (Prices == null) {alert("ERROR - Handelsstatistik - Load Prices failed"); return 33;}
  
  
  
  if (document.getElementById("drag_box"))
  {      
    //Enthält die angezeigten Tage (wird aus Tabellenanzeige errechnet: var d1ticks)
    var SelectedDays = Math.ceil((/""\],\[(\d+),""\]\];/.exec(document.getElementById("drag_box").innerHTML)[1])/24);

    var startDate = new Date(LastUpdate.Year,LastUpdate.Month,LastUpdate.Day,0,0,0),
   				Today = new Date(Number(Servertime[2]),Number(Servertime[1])-1,Number(Servertime[0]),0,0,0), //Date(year,month,day,Hour,minuit,sec) reminder: Month-1 (so 0 is January)
  			  DaysDiff = (Today.getTime() - startDate.getTime())/(60000*60*24);
    
    if (SelectedDays == 7 && DaysDiff>0)
    {
      if (DaysDiff>6) DaysDiff = 6; //max last 6 Days in Data
      var LastMidnight = SelectedDays*24-(Math.ceil((Number(Servertime[3])+1)/3)*3);
      
      
      for (ResNum in ResList)
      {
        var AvaPrice = 0;
        for (var i = 0; i < DaysDiff; i++)
        {
          var Price = 0;
          for (var Hour = LastMidnight-24*i; Hour > LastMidnight-24*(i+1); Hour-=3)
            {
              Price += Number(GetPrice (ResList[ResNum],Hour));
            }

          AvaPrice +=Price/8;
        }
        
        Prices[ResList[ResNum]] = (Prices[ResList[ResNum]]*Prices.Number+AvaPrice)/(Prices.Number+DaysDiff);
      }
      Prices.Number += DaysDiff;
      
      //Update finish, set new Timestamp
      GM.setValue('Prices', JSON.stringify(Prices));
		  GM.setValue('LastUpdate', JSON.stringify({Year:Number(Servertime[2]),Month:Number(Servertime[1])-1,Day:Number(Servertime[0])}));
   	}
  }
  
  
  //Einfügen eines neuen Reiters
  if (rand)
  { 
    //S hinzufügen
    var StatTab = document.createElement("li");
    StatTab.title = "Statistik Übersicht";
    var a = document.createElement("a");
    var ai = document.createTextNode("S");
    a.appendChild(ai);
    a.className = "inactive";
    a.style.cursor = "pointer";
    a.id = "StatTab";
    StatTab.appendChild(a);
    rand.getElementsByTagName("ul")[0].appendChild(StatTab);
  
    //Konfigurationsseite
    var Stat = document.createElement("div");
    Stat.id = "Stat";
    Stat.style.display = "none";
    Stat.style.width = "160px";
    Stat.style.marginTop = "2px";
    Stat.style.marginBottom = "20px";
    Stat.style.marginLeft = "7px";
    rand.insertBefore(Stat, rand.getElementsByTagName("ul")[0].nextSibling);
    var liste = "<b>Durchschnittspreise:<\/b><br\/><br\/>"
    						+"<table class=\"store\">"
    						+"<tr><td><a href=\"?t=market&product=alraune\" class=\"inline\">Alraune:<\/a><\/td><td>"+Math.round(Prices.alraune*10)/10.0+"<\/td><\/tr>"
    						+"<tr><td><a href=\"?t=market&product=bretter\" class=\"inline\">Bretter:<\/a><\/td><td>"+Math.round(Prices.bretter*10)/10.0+"<\/td><\/tr>"		
                +"<tr><td><a href=\"?t=market&product=brot\" class=\"inline\">Brot:<\/a><\/td><td>"+Math.round(Prices.brot*10)/10.0+"<\/td><\/tr>"
    						+"<tr><td><a href=\"?t=market&product=eisen\" class=\"inline\">Eisen:<\/a><\/td><td>"+Math.round(Prices.eisen*10)/10.0+"<\/td><\/tr>"
        				+"<tr><td><a href=\"?t=market&product=eisenerz\" class=\"inline\">Eisenerz:<\/a><\/td><td>"+Math.round(Prices.eisenerz*10)/10.0+"<\/td><\/tr>"
    						+"<tr><td><a href=\"?t=market&product=zauberwasser\" class=\"inline\">Elixier:<\/a><\/td><td>"+Math.round(Prices.zauberwasser*10)/10.0+"<\/td><\/tr>"
        				+"<tr><td><a href=\"?t=market&product=faesser\" class=\"inline\">Fässer:<\/a><\/td><td>"+Math.round(Prices.faesser*10)/10.0+"<\/td><\/tr>"
    						+"<tr><td><a href=\"?t=market&product=fleisch\" class=\"inline\">Fleisch:<\/a><\/td><td>"+Math.round(Prices.fleisch*10)/10.0+"<\/td><\/tr>"
    						+"<tr><td><a href=\"?t=market&product=gemuese\" class=\"inline\">Gemüse:<\/a><\/td><td>"+Math.round(Prices.gemuese*10)/10.0+"<\/td><\/tr>"
    						+"<tr><td><a href=\"?t=market&product=geschirr\" class=\"inline\">Geschirr:<\/a><\/td><td>"+Math.round(Prices.geschirr*10)/10.0+"<\/td><\/tr>"
    						+"<tr><td><a href=\"?t=market&product=getreide\" class=\"inline\">Getreide:<\/a><\/td><td>"+Math.round(Prices.getreide*10)/10.0+"<\/td><\/tr>"
    						+"<tr><td><a href=\"?t=market&product=hanf\" class=\"inline\">Hanf:<\/a><\/td><td>"+Math.round(Prices.hanf*10)/10.0+"<\/td><\/tr>"
    						+"<tr><td><a href=\"?t=market&product=holz\" class=\"inline\">Holz:<\/a><\/td><td>"+Math.round(Prices.holz*10)/10.0+"<\/td><\/tr>"
    						+"<tr><td><a href=\"?t=market&product=honig\" class=\"inline\">Honig:<\/a><\/td><td>"+Math.round(Prices.honig*10)/10.0+"<\/td><\/tr>"
    						+"<tr><td><a href=\"?t=market&product=kerzen\" class=\"inline\">Kerzen:<\/a><\/td><td>"+Math.round(Prices.kerzen*10)/10.0+"<\/td><\/tr>"
    						+"<tr><td><a href=\"?t=market&product=kohle\" class=\"inline\">Kohle:<\/a><\/td><td>"+Math.round(Prices.kohle*10)/10.0+"<\/td><\/tr>"
    						+"<tr><td><a href=\"?t=market&product=leder\" class=\"inline\">Leder:<\/a><\/td><td>"+Math.round(Prices.leder*10)/10.0+"<\/td><\/tr>"
    						+"<tr><td><a href=\"?t=market&product=lehm\" class=\"inline\">Lehm:<\/a><\/td><td>"+Math.round(Prices.lehm*10)/10.0+"<\/td><\/tr>"
    						+"<tr><td><a href=\"?t=market&product=mehl\" class=\"inline\">Mehl:<\/a><\/td><td>"+Math.round(Prices.mehl*10)/10.0+"<\/td><\/tr>"
    						+"<tr><td><a href=\"?t=market&product=met\" class=\"inline\">Met:<\/a><\/td><td>"+Math.round(Prices.met*10)/10.0+"<\/td><\/tr>"
    						+"<tr><td><a href=\"?t=market&product=moebel\" class=\"inline\">Möbel:<\/a><\/td><td>"+Math.round(Prices.moebel*10)/10.0+"<\/td><\/tr>"
    						+"<tr><td><a href=\"?t=market&product=naegel\" class=\"inline\">Nägel:<\/a><\/td><td>"+Math.round(Prices.naegel*10)/10.0+"<\/td><\/tr>"
    						+"<tr><td><a href=\"?t=market&product=obst\" class=\"inline\">Obst:<\/a><\/td><td>"+Math.round(Prices.obst*10)/10.0+"<\/td><\/tr>"
    						+"<tr><td><a href=\"?t=market&product=papier\" class=\"inline\">Papier:<\/a><\/td><td>"+Math.round(Prices.papier*10)/10.0+"<\/td><\/tr>"
    						+"<tr><td><a href=\"?t=market&product=sattel\" class=\"inline\">Sättel:<\/a><\/td><td>"+Math.round(Prices.sattel*10)/10.0+"<\/td><\/tr>"
    						+"<tr><td><a href=\"?t=market&product=seile\" class=\"inline\">Seile:<\/a><\/td><td>"+Math.round(Prices.seile*10)/10.0+"<\/td><\/tr>"
    						+"<tr><td><a href=\"?t=market&product=folianten\" class=\"inline\">Spruchrollen:<\/a><\/td><td>"+Math.round(Prices.folianten*10)/10.0+"<\/td><\/tr>"
    						+"<tr><td><a href=\"?t=market&product=steine\" class=\"inline\">Steine:<\/a><\/td><td>"+Math.round(Prices.steine*10)/10.0+"<\/td><\/tr>"
    						+"<tr><td><a href=\"?t=market&product=steinziegel\" class=\"inline\">Steinziegel:<\/a><\/td><td>"+Math.round(Prices.steinziegel*10)/10.0+"<\/td><\/tr>"
    						+"<tr><td><a href=\"?t=market&product=stoff\" class=\"inline\">Stoff:<\/a><\/td><td>"+Math.round(Prices.stoff*10)/10.0+"<\/td><\/tr>"
    						+"<tr><td><a href=\"?t=market&product=vieh\" class=\"inline\">Vieh:<\/a><\/td><td>"+Math.round(Prices.vieh*10)/10.0+"<\/td><\/tr>"
    						+"<tr><td><a href=\"?t=market&product=werkzeuge\" class=\"inline\">Werkzeuge:<\/a><\/td><td>"+Math.round(Prices.werkzeuge*10)/10.0+"<\/td><\/tr>"
    						+"<tr><td><a href=\"?t=market&product=ziegel\" class=\"inline\">Ziegel:<\/a><\/td><td>"+Math.round(Prices.ziegel*10)/10.0+"<\/td><\/tr>"
    						+"<tr><td colspan=\"2\" ><\/td><\/tr>"
    						+"<tr><td colspan=\"2\" ><b>Mittelwert über "+Prices.Number+" Tage<\/b><\/td><\/tr>"
    						+"<\/table>";

    Stat.innerHTML = liste;
    if (/&t=market&/.exec(window.location.href)){document.getElementById("StatTab").addEventListener("click", zeigen, false);}
    else {zeigen();}
  }
  
  //bei Klick auf S Konfiguration öffnen
  function zeigen()
  {
    if (document.getElementById("StatTab").className == "inactive")
    {
      Stat.style.display = "block";
      var lis = rand.getElementsByTagName("ul")[0].getElementsByTagName("li");
      for (k=0;k<lis.length;k++)
      {
        lis[k].getElementsByTagName("a")[0].className = "inactive";
      }
      document.getElementById("StatTab").className = "active";
      rand.innerHTML = rand.innerHTML.replace(/(<span class=\"end\"><\/span><\/div>)/, "$1\n<div id=\"randInhalt\" "+"style=\"display:none;\">");
      rand.innerHTML = rand.innerHTML.replace(/(<div class=\"mainNavi\" >)/, "<\/div>\n$1");
    }
  }
  })();//end of async
}



//Einfügen der Einkaufspreise
//nur wenn 'Angebot' aktiviert
if (document.getElementById("market_buy"))
{  
  
  
  (async () => {
  
  let Prices = JSON.parse(await GM.getValue('Prices',null));
  if (Prices == null) {alert("ERROR - Handelsstatistik - Load Prices failed"); return 33;}
   
  for (ResNum in ResList){
  	if (document.getElementById("go_"+ResList[ResNum])){document.getElementById("market_buy").innerHTML = document.getElementById("market_buy").innerHTML.replace("Bester Nachfragepreis", "Langjähriger Durchschnitt: "+Math.round(Prices[ResList[ResNum]]*10)/10.0+"</td></tr><tr><td colspan=4 class=\"annotation\">Bester Nachfragepreis");
     																										document.getElementById("go_"+ResList[ResNum]).value=Math.ceil(Prices[ResList[ResNum]]*(1+Offer));
                                              					break;}
  	}
  })();//end of async
  
  
}

//Einfügen der Einkaufspreise
//nur wenn 'Nachfrage' aktiviert
if (document.getElementById("market_sell"))
{
	(async () => {
    
  let Prices = JSON.parse(await GM.getValue('Prices',null));
  if (Prices == null) {alert("ERROR - Handelsstatistik - Load Prices failed"); return 33;}   

  for (ResNum in ResList){
  	if (document.getElementById("gd_"+ResList[ResNum])){document.getElementById("market_sell").innerHTML = document.getElementById("market_sell").innerHTML.replace("Bester Angebotspreis", "Langjähriger Durchschnitt: "+Math.round(Prices[ResList[ResNum]]*10)/10.0+"</td></tr><tr><td colspan=4 class=\"annotation\">Bester Angebotspreis");
     																										document.getElementById("gd_"+ResList[ResNum]).value=Math.floor(Prices[ResList[ResNum]]*(1-Demand));
                                              					break;}
  	}
	})();//end of async
}


//default
if (/tab=13&setHandelsstatistik/.exec(window.location.href))
{
  GM.setValue('LastUpdate', JSON.stringify({Year:2020,Month:2,Day:16}));
  GM.setValue('Prices', JSON.stringify({Number:444,
                                        alraune:60.8,
                                        bretter:42.6,
                                        brot:84.4,
                                        eisen:91.5,
                                        eisenerz:36.3,
                                        zauberwasser:947.9,
                                        faesser:459.0,
                                        fleisch:432.0,
                                        gemuese:68.0,
                                        geschirr:347.7,
                                        getreide:20.7,
                                        hanf:48.9,
                                        holz:12.6,
                                        honig:20.2,
                                        kerzen:268.2,
                                        kohle:28.2,
                                        leder:258.9,
                                        lehm:17.4,
                                        mehl:36.2,
                                        met:298.7,
                                        moebel:97.5,
                                        naegel:239.8,
                                        obst:101.5,
                                        papier:299.1,
                                        sattel:554.4,
                                        seile:142.8,
                                        folianten:1088.1,
                                        steine:20.1,
                                        steinziegel:79.1,
                                        stoff:277.5,
                                        vieh:60.1,
                                        werkzeuge:418.5,
                                        ziegel:83.4
                                       }));
	alert("Werte resetet!");
}