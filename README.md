# Dragosien Handelsstatistik

Greasemonkey Erweiterung für das Browsergame [**Dragosien - Land der Drachen**](http://www.dragosien.de).<br>
Folgende Vorteile bietet dieses Paket:

* Automatische Errechnung der langjährigen Durchschnittspreise aller Waren
* Anzeige aller langjährigen Durchschnittspreise
* Automatisches Ausfüllen der Nachfrage- und Angebotspreise mit den Durchschnittspreisen zuzüglich eines Boni

Hierdurch können langfristige Kursschwankungen ausgenutzt und maximale Gewinne erwirtschaftet werden.


## Setup

1. Greasemonkey öffnen
2. Neues Benutzerskript erstellen
3. `GMScript.js` komplett kopieren und in neues Benutzerskript einfügen
4. In Dragosien einloggen
5. `http://www.dragosien.de/?t=market&tab=13&setHandelsstatistik` kopieren, in die Browseradresszeile einfügen und ausführen (dies dient zur Initialisierung der ersten Werte)

## Anwendung

### Aktualisierung der Durchschnittspreise

Durch Schritt 5 der Setup-Beschreibung werden die Durchschnittswerte zwischen Sommer 2018 und Frühjahr 2020 geladen.<br>

Um die Preise weiterhin zu aktualisieren sollte mindestens einmal in der Woche im Markt der Reiter `Statistiken` aufgerufen werden.
Hierdurch werden die aktuellen Werte zu den Durchschnittspreisen hinzugerechnet.<br>

In diesem Reiter erscheinen zusätzlich im rechten Fenster alle Durchschnittspreise sowie die Anzahl der bisher gesammelten Tage.

### Änderungen der Angebots- und Nachfrageseite

In den entsprechenden Angebots- und Nachfragefenstern wird nun der langjährige Durchschnitt angezeigt.<br>

Zusätzlich wird automatisch das Feld mit dem Angebots- bzw. Nachfragepreis automatisch ausgefüllt. Hierbei wird standardmäßig ein Preis 6% über bzw. unter dem Durchschnittspreis eingesetzt.
Diese Werte können von Hand im Skript in Zeile 11 und 12 entsprechend angepasst werden.<br>

ACHTUNG: Auch wenn das Feld bereits ausgefüllt ist, können nur Angebote und Nachfragen innerhalb der angegebenen Preisspanne abgegeben werden.

### Eigene Durchschnittswerte eintragen

Selbstverständlich können auch eigene Aufzeichnungen der Preise eingefügt werden.
Hierzu im Skript ganz nach unten gehen und die Werte entsprechend anpassen.
`Number` ist dabei die Anzahl an Tagen die für den Durchschnittswert angenommen wurden.<br>

Eine Aufsplittung einzelner Waren in eine längere Aufzeichnungsdauer ist nicht möglich.<br>

Nach dem abspeichern wieder `http://www.dragosien.de/?t=market&tab=13&setHandelsstatistik` aufrufen um die neuen Werte zu laden.

## Haftungsausschluss

Der Autor garantiert nicht für Aktualität, Korrektheit und Funktionalität des Skripts oder sonstige auftretende Probleme.<br>

Bei Problemen und Fragen gerne Ingame an `Wewwer` wenden.